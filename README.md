## Functionalities offered by Task List: ##

* Add tasks to your task list using 'Add Task' button.
* Check 'Important' to highlight the task.
* Click on 'Edit' button to edit a Task. 
* Click 'Complete' to move a task to completed list.    


### How to Complete a Task? ###

* Click on 'Edit' button and in the dropdown, click on 'Complete' button.
* Once clicked the task will be moved to completed tasks list.
* You can remove the tasks from the completed tasks list using 'Remove' button.
* Clicking on 'Remove' will permanently delete the task.

### How to remove Important Highlighting? ###

* Click on 'Edit' button and in the dropdown click on 'Not Important' button.

### How to check description of a Task ###

* Click on the heading/name of the task which will open a dropdown with the description details.

### How to edit the description? ###

* Click on 'Edit' button and in the dropdown you can directly edit the description block.
* Editted details will be autosaved.